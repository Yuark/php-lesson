<?php
	/*include();
	include_once();
	require();*/
	require_once('function.php');

	$navigation = [
		'#main' => 'Главная',
		'#protein' => 'Протеин',
		'#bcaa' => 'BCAA',
		'#peptids' => 'Пептиди',
		'#contacts' => 'Контакти'
	];
	
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		nav {
			display: flex;
		}
		nav div { padding: 10px; }
		nav div a { color: green; }
		nav div a:hover { text-decoration: none;}
	</style>
</head>
<body>
	<nav>
		<?php
			showNav($navigation);
		?>
	</nav>
</body>
</html>